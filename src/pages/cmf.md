---
title: BRAM
---



# BRAM CMF Application

Authors: Armin, Shervin, Carlos




## Innovations in Creativity

...



## Innovation in Technology

Games are beautiful. Like other media, they can take us into awe-inspiring worlds filled with action and adventure and tell us rich stories of a character's struggles and personal development. But unlike any other form of entertainment video games put us, the player, at the helm. We are the ones exploring an alien world teeming with fear and excitment, or making the difficult choice to save a friend at the cost of losing another. Video games can tell a story, but more importantly, they allow us to co-author it. At Vampire Twin Studios, this is the foundation of our ethos as game developers. We strive to tell rich and engaging stories that make a true and lasting impact on our players.

The bridge between presenting a story and empowering the player to make it their own is constructed by the game mechanics we design and the technology that builds it.

When designing Bram, we understand how crucial the marraige between artistic visuals and an engaging gameplay is. We strive to preserve the nostalgic, 2D art that we love and cherish while maintaining the modern sensibilities of engaging and fun gameplay. To do so, we innovated on several technologies to achieve this delicate balance.

### 1. A 2D World Within A 3D World

The 2D games of yesteryear leaves us with nostalgic memories of our childhood. There is something so compelling about 2D art that is meant to represent the 3D world that we live in. In Bram, we wanted to preserve this aesthetic charm of pixel art while leveraging modern technologies to create a game that is 

Central to our innovative approach is the adoption of a novel 2.5D rendering technique to evoke the aesthetic charm of retro pixel art games while affording players the dynamic gameplay mechanics synonymous with modern action games. Leveraging a fusion of orthographic camera principles and skewed projection matrices, our game, [Game Title], seamlessly blends 2D sprite art with pseudo-3D object rendering on a 45-degree plane. This innovative rendering methodology enables us to harness the power of Unity's physics engine and three-dimensional z-order rendering, facilitating the realization of intricate combat systems and interactive environmental elements previously unattainable within the confines of traditional 2D game design paradigms. By marrying nostalgic visual aesthetics with cutting-edge gameplay mechanics, we transcend the limitations of conventional game development frameworks, offering players an immersive and captivating gaming experience that transcends the boundaries of time and technology.
[lol](/img/bram-renderedworld-1.png)

<div style={{display: 'flex'}}>
    <div style={{flex: '50%', padding: '0 5px'}}>
        <img src={require('@site/static/img/bram-renderedworld-1.png').default} alt="Real World"/>
        <p><em><u>Figure 1:</u> Rendered World</em></p>
    </div>
    <div style={{flex: '50%', padding: '0 5px'}}>
        <img src={require('@site/static/img/bram-realworld-1.gif').default} alt="Real World"/>
        <p><em><u>Figure 2:</u> Welcome, to the <strong>real</strong> world</em></p>
    </div>
</div>



### 2. An Ever Changing World

A cornerstone of our innovation lies in the implementation of procedural level generation, facilitated by the integration of the "Edgar Pro - Procedural Level Generator," a groundbreaking tool developed by Ondrej Nepozitek. This tool combines a graph-based approach with meticulously crafted room templates, granting game designers unprecedented control over the generation of 2D levels. As one of the early adopters of this tool, we recognized its potential to revolutionize level design within the context of our 2.5D game, [Game Title]. Collaborating closely with Ondrej, we provided invaluable feedback and feature suggestions, contributing to the refinement and eventual release of the tool on the Unity Asset Store. Our utilization of procedural generation strikes a delicate balance between randomness and pre-authored design elements, ensuring that each gameplay experience feels fresh and dynamic while retaining a sense of crafted cohesion. By eschewing true randomness in favor of curated unpredictability, we elevate the player's engagement and immersion, a testament to our commitment to innovative game design methodologies.





modern sensibilities

hand craft spaces that environments that evoke 

2D / 3D 